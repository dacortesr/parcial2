<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/TodoDAO.php';
class Todo
{
    private $region;
    private $codigo;
    private $nombre;
    private $casos_acum;
    private $muertes_acum;
    private $casos_utlimo;
    private $muertes_ultimo;
    private $conexion;
    private $todoDAO;

    public function getRegion()
    {
        return $this->region;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }
    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCasos_acum()
    {
        return $this->casos_acum;
    }
    public function getMuertes_acum()
    {
        return $this->muertes_acum;
    }
    public function getMuertes_ultimo()
    {
        return $this->muertes_ultimo;
    }
    public function getCasos_ultimo()
    {
        return $this->casos_utlimo;
    }
    

    


    public function __construct($region = "", $codigo = "", $nombre = "", $casos_acum = "",$muertes_acum = "",$casos_utlimo="",$muertes_ultimo ="")
    {
        $this->region = $region;
        $this->codigo = $codigo;
        $this->nombre = $nombre;
        $this->casos_acum = $casos_acum;
        $this->muertes_acum = $muertes_acum;
        $this->casos_utlimo = $casos_utlimo;
        $this->muertes_utlimo = $muertes_ultimo;
        $this->conexion = new conexion();
        $this->todoDAO = new TodoDAO($this->region, $this->codigo, $this->nombre, $this->casos_acum, $this->muertes_acum, $this->casos_utlimo, $this->muertes_ultimo);
    }

    public function verdatos($filtro){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->todoDAO->ver($filtro));
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Todo($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $registro[6]);
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }
}
