<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/PaisDAO.php';
class Pais
{
    private $id;
    private $nombre;
    private $id_region;
    private $paisDAO;

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getId_region()
    {
        return $this->id_region;
    }


    public function __construct($id = "", $nombre = "", $id_region = "")
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->precio = $id_region;
        $this->conexion = new conexion();
        $this->paisDAO = new PaisDAO($this->id, $this->nombre, $this->id_region);
    }

    public function verpais(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->paisDAO->ver());
        $cate = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $cas = new Pais($registro[0], $registro[1],"");
            array_push($cate, $cas);
        }
        $this->conexion->cerrar();
        return  $cate;
    }
}
?>