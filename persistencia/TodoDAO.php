<?php

class TodoDAO
{
    private $region;
    private $codigo;
    private $nombre;
    private $casos_acum;
    private $muertes_acum;
    private $casos_utlimo;
    private $muertes_ultimo;
    


    public function __construct($region, $codigo, $nombre, $casos_acum, $muertes_acum, $casos_utlimo, $muertes_ultimo)
    {
        $this->region = $region;
        $this->codigo = $codigo;
        $this->nombre = $nombre;
        $this->casos_acum = $casos_acum;
        $this->muertes_acum = $muertes_acum;
        $this->casos_utlimo = $casos_utlimo;
        $this->muertes_utlimo = $muertes_ultimo;
    }

    public function ver($filtro){
        return "select r.name, r.id_region, c.name,re.cumulative_cases,re.cumulative_deaths,
        re.new_cases,re.new_deaths from region r JOIN country c on (r.id_region = c.id_region_region) 
        JOIN report re on (c.id_country = re.id_country_country) where c.name = '".$filtro."'";
    }

}
