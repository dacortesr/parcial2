<?php
require_once 'logica/Todo.php';
$filtro = $_GET["filtro"];
$Dato = new Todo();
$Datos = $Dato -> verdatos($filtro);

?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Datos covid 19</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Region</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Casos Acumulados</th>
                                <th scope="col">Muertes Acumuladas</th>
                                <th scope="col">Casos del ultimo dia reportado</th>
                                <th scope="col">Muertes del ultimo dia reportado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($Datos as $lib) {

                                echo "<tr>";
                                echo "<td>" . $lib->getRegion() . "</td>";
                                echo "<td>" . $lib->getCodigo() . "</td>";
                                echo "<td>" . $lib->getNombre() . "</td>";
                                echo "<td>" . $lib->getCasos_acum() . "</td>";
                                echo "<td>" . $lib->getMuertes_acum() . "</td>";
                                echo "<td>" . $lib->getCasos_ultimo() . "</td>";
                                echo "<td>" . $lib->getMuertes_ultimo() . "</td>";
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>