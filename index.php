<?php
require_once 'logica/Pais.php';
$Pais = new Pais();
$verp = $Pais->verpais();
?>

<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

</head>

<div class="row">
    <div class="col 3">

    </div>
    <div class="col 6 text-center">
        <h1>Casos de covid-19 </h1>
        <div class="input-group">
            <div class="input-group-prepend">
                <label class="input-group-text"> Pais </label>
            </div>
            <select id="filtro" class="form-select form-select-sm" aria-label=".form-select-sm example">
                <?php
                foreach ($verp as $v) {
                    echo "<option  value = '" . $v->getId() . "'>" . $v->getNombre() . "</option>";
                }
                ?>

            </select>
            <input type="text" id="eso" />
        </div>


    </div>
    <div class="col 3">

    </div>
</div>
<div class="row">
    <div class="col">
        <div id="resultados"></div>
    </div>
</div>
<script>
    $("#filtro").change(function() {
        var h = $("#filtro option:selected").text();
        $("#eso").val(h);
    });
    $("#eso").keyup(function() {
        filtro = $("#filtro option:selected").text();
        url = "principal.php?filtro=" + filtro;
        $("#resultados").load(url);
    });
</script>

</html>